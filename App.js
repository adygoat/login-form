import React from 'react';
import { View, StyleSheet } from 'react-native';
import SignupForm from './components/SignUpForm'; 
import ProgressReport from './components/ProgressReport';
import ProgressReportDump from './components/ProgressReportDump';

const App = () => {
  return (
    <View style={styles.container}>
      <ProgressReport/> 
     
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default App;