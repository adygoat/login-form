import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
} from 'react-native';
import axios from 'axios';
import { BarChart } from 'react-native-chart-kit';

export default function ProgressReportDump() {
  const [journalData, setJournalData] = useState([]);
  const userId = 12345; // Replace with the actual user ID

  useEffect(() => {
    // Fetch journal data for the specific user and set it to journalData state
    axios.get(`http://localhost:8080/getByUser?userId=${userId}`) 
      .then((response) => {
        setJournalData(response.data);
      })
      .catch((error) => {
        console.error('Error fetching journal data:', error);
      });
  }, [userId]); // Include userId in the dependency array to re-fetch data when it changes

  const getMonthName = (date) => {
    const options = { month: 'short' };
    return new Date(date).toLocaleDateString(undefined, options);
  };

  // Process journal data and create a tally by month
  const tallyJournalsByMonth = () => {
    const tally = {};
    journalData.forEach((journal) => {
      const journalDate = new Date(journal.date);
      const month = `${journalDate.getFullYear()}-${(journalDate.getMonth() + 1)
        .toString()
        .padStart(2, '0')}`;

      if (tally[month]) {
        tally[month]++;
      } else {
        tally[month] = 1;
      }
    });

    const tallyArray = Object.keys(tally).map((month) => ({
      month: new Date(`${month}-01`), // Use Date object for sorting
      count: tally[month],
    }));

    // Sort the tallyArray by date (chronological order)
    tallyArray.sort((a, b) => a.month - b.month);

    return tallyArray;
  };

  const journalTally = tallyJournalsByMonth();

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.chartContainer}>
        <Text style={styles.chartTitle}>Journals Made by Month</Text>
        <BarChart
          data={{
            labels: journalTally.map((entry) => getMonthName(entry.month)), // Format months for labels
            datasets: [
              {
                data: journalTally.map((entry) => entry.count),
              },
            ],
          }}
          width={Dimensions.get('window').width - 32} // Adjust the width
          height={250}
          yAxisSuffix=" journals"
          chartConfig={{
            backgroundColor: '#ffffff',
            backgroundGradientFrom: '#ffffff',
            backgroundGradientTo: '#ffffff',
            decimalPlaces: 0,
            color: (opacity = 1) => `rgba(64, 224, 208, ${opacity})`,
            labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            style: {
              borderRadius: 16,
            },
            propsForDots: {
              r: '6',
              strokeWidth: '2',
              stroke: '#30d5c8',
            },
            yLabels: [0, 5, 10, 15, 20, 25],
            labelFontSize: 8, // Adjust font size
          }}
          fromZero
          yAxisInterval={5}
        />
      </View>
    </ScrollView>
  );
}

const styles = {
  container: {
    flex: 1,
    padding: 16,
  },
  chartContainer: {
    backgroundColor: '#ffffff', // Adjust the background color
    padding: 10,
    borderRadius: 16,
    margin: 10, // Add margin to the chart container
  },
  chartTitle: {
    fontSize: 18, // Adjust font size
    fontWeight: 'bold',
    marginBottom: 16,
  },
};