import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
} from 'react-native';
import axios from 'axios';
import { BarChart } from 'react-native-chart-kit';

export default function ProgressReport() {
  const [journalData, setJournalData] = useState([]);
  const userId = 12345; // Replace with the actual user ID (partial)
  /* const userId = route.params.userid;  <--------- actual code*/

  useEffect(() => {
    // Fetch journal data for the specific user and set it to journalData state
    axios.get(`http://localhost:8080/getByUser?userId=${userId}`)
      .then((response) => {
        setJournalData(response.data);
      })
      .catch((error) => {
        console.error('Error fetching journal data:', error);
      });
  }, [userId]); // Include userId in the dependency array to re-fetch data when it changes

  const getMonthName = (date) => {
    const options = { month: 'short' };
    return new Date(date).toLocaleDateString(undefined, options);
  };

  // Process journal data and create a tally by month
  const tallyJournalsByMonth = () => {
    const tally = {};
    journalData.forEach((journal) => {
      const journalDate = new Date(journal.date);
      const month = `${journalDate.getFullYear()}-${(journalDate.getMonth() + 1)
        .toString()
        .padStart(2, '0')}`;

      if (tally[month]) {
        tally[month]++;
      } else {
        tally[month] = 1;
      }
    });

    
    // Create an array of months for the year 2023
    const monthsForYear = Array.from({ length: 12 }, (_, index) => {
      return new Date(2023, index, 1);
    });

    // Create an array with counts for each month
    const tallyArray = monthsForYear.map((month) => ({
      month,
      count: tally[`${month.getFullYear()}-${(month.getMonth() + 1).toString().padStart(2, '0')}`] || 0,
    }));

    return tallyArray;
  };

  const journalTally = tallyJournalsByMonth();

  const formatYLabel = (value) => {
    return value === 1 ? '1 journal' : `${value} journals`;
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>Progress Report</Text>
      </View>
      <View style={styles.chartContainer}>
        <Text style={styles.chartTitle}>Journals Made by Month</Text>
        <BarChart
          data={{
            labels: journalTally.map((entry) => getMonthName(entry.month)),
            datasets: [
              {
                data: journalTally.map((entry) => entry.count),
              },
            ],
          }}
          width={Dimensions.get('window').width - 1350}
          height={250}
          yAxisSuffix={journalTally[0]?.count === 1 ? ' journal' : ' journals'}
          chartConfig={{
            backgroundColor: '#ffffff',
            backgroundGradientFrom: '#ffffff',
            backgroundGradientTo: '#ffffff',
            decimalPlaces: 0,
            color: (opacity = 1) => `rgba(64, 224, 208, ${opacity})`,
            labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            style: {
              borderRadius: 16,
            },
            propsForDots: {
              r: '6',
              strokeWidth: '2',
              stroke: '#30d5c8',
            },
            yLabels: [0, 5, 10, 15, 20, 25],
            labelFontSize: 8,
          }}
          fromZero
          yAxisInterval={2}
        />
        <Text style={styles.chartTitle}>Here is the progress report for the year.</Text>
      </View>
    </ScrollView>
  );
}

const styles = {
  container: {
    flex: 1,
    padding: 8,
  },
  header: {
    backgroundColor: 'turquoise',
    padding: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
  },
  chartContainer: {
    backgroundColor: '#ffffff',
    padding: 8,
    borderRadius: 16,
    margin: 10,
  },
  chartTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 16,
  },
};
